//
// DLS
//
// It all starts here.

import Obj from './core/obj';
import EventAggregator from './core/events';
import Platform from './platform/platform';
import Util from './util/util';
import Components from './components/components';

export default class DLSApp extends Obj {

	constructor (opts) {
		super(opts);

		this.prepare();

		Obj.prototype.app = this;
		Obj.prototype.scope = $(this.scope);

		this.createVent();
		this.registerPlatform();
		this.createUtils();

		this.listen();
	}

	// Prepare
	prepare() {
		if(!this.scope)
			this.scope = $('body');
	}

	// Create the vent event aggregator that we can
	// subscribe and publish events to. A la Marionette
	createVent() {
		this.vent = new EventAggregator();
	}

	// Register Platform
	registerPlatform() {
		Obj.prototype.platform = new Platform();
	};

	// Create the utility helpers
	createUtils() {
		Obj.prototype.util = new Util({
			vent: this.vent
		});
	}

	// Bind our key events
	listen() {
		this
			.on('before:start', e => {
				this.beforeStart();
			})
			.on('after:start', e => {
				this.afterStart();
			});
	}

	// Before Start tasks
	beforeStart() {

	}

	// After start tasks
	afterStart() {

	}

	// Start the application
	start () {
		this.emit('before:start');
		this.initialize();
		this.emit('after:start');
	}

	// The initial render, should only occur internally to
	// start the modules
	initialize() {
		this.createComponents();
		this.createModules();
	}

	// Manually render DLS
	// Accessible via DLS.trigger('render', {opts});
	// or DLS.render({opts})
	render(...args) {
		this.trigger('render');
		return this;
	}

	// Shorthand to create component
	create(type, params) {
		if(!this.component[type]) {
			console.warn(`DLS: Component of type \`${type}\` does not exist!`);
			return this;
		}

		return this.component[type].create(params);
	}

	// Create the components
	createComponents() {
		this.component = new Components();
	}

	// Create and start all the supplied app modules
	createModules() {
		$.each(this.modules, (key, Module) => {
			this.modules[key] = new Module();
		});
	}
}
