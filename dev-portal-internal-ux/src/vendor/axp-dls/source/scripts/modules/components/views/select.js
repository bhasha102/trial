//
// Select Component
//

import View from '../../core/view';
import Component from '../classes/component';
import SelectModel from '../models/select.js';

//
// Configuration
//
const Name = 'Select';
const Selector = '[data-toggle="select"]:not([data-ignore])';
const DataKey = 'dls.select';
const EventKey = `.${DataKey}`;

//
// Initiator
//
export default class Select extends Component {

	constructor (opts) {
		super(opts);
		this.component = SelectView;

		this.render();
	}
}

//
// Select View
//
class SelectView extends View {

	constructor (opts) {
		super(opts);
		this.render();
		this.listen();
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {return Selector;}
	static get EventKey() {return EventKey;}

	//
	// Render
	//
	render() {
		this.target
			.attr('data-rendered', true)
			.attr('data-value', this.select.val());
	}

	//
	// Listen
	//
	listen() {
		this.select
			.on('change', () => {
				this.model.set('value', this.select.val());
				this.target.attr('data-value', this.select.val());
			})
			.on('focus', () => {
				this.target.addClass('focus');
			})
			.on('blur', () => {
				this.target.removeClass('focus');
			});
	}


	//
	// Interface
	//
	static _interface(config) {
		let $el = $(this),
			$select = $el.find('select'),
			data = $el.data(DataKey);

		config = Object.assign($el.data(), config);
		config.value = $el.find('select').val();

		if(!data) {
			data = new SelectView({
				target: $el,
				select: $select,
				model: new SelectModel(config)
			});

			$el.data(DataKey, data);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
