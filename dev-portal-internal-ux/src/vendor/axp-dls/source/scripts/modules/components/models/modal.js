//
// Modal Model
//

import Model from '../../core/model';

export default class ModalModel extends Model {}

ModalModel.prototype.defaults = {
	theme: 'default',
	classes: '',
	controlsLeft: 'back',
	controlsRight: '',
	size: 'fill', // fill, dynamic or boxed,
	title: 'Test',
	subtitle: '',
	content: '',
	animation: 'fade',
	container: $('body'),
	open: false
};
