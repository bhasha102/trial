//
// Components
// Manages the creation of DLS components
//

import Obj from '../core/obj';
import Select from './views/select';
import Accordion from './views/accordion';
import Collapse from './views/collapsible';
import Dismiss from './views/dismissible';
import Dropdown from './views/dropdown';
import Modal from './views/modal';
import Search from './views/search';
import Slider from './views/slider';
import Stepper from './views/stepper';
import Tabs from './views/tabs';
import Tooltip from './views/tooltip';
import SmartField from './views/smartfield';
import Carousel from './views/carousel';

export default class Components extends Obj {

	constructor (opts) {
		super(opts);

		this.component = {
			accordion: new Accordion(),
			collapse: new Collapse(),
			dismiss: new Dismiss(),
			dropdown: new Dropdown(),
			modal: new Modal(),
			search: new Search(),
			select: new Select(),
			slider: new Slider(),
			stepper: new Stepper(),
			tabs: new Tabs(),
			tooltip: new Tooltip(),
			smartfield: new SmartField(),
			carousel: new Carousel()
		};

		this.register();
		this.listen();
	}

	//
	// Register the components (for external access)
	//
	register() {
		for(let key in this.component) {
			this[key] = this.component[key];
		}
	}

	//
	// Bind the listeners
	//
	listen() {
		this.app
			.on('render', (...args) => {
				this.render(args);
			});
	}

	// Render the components asynchronously
	// (generally would not be used but may be used in a web-app situation
	// where components may be added dynamically).
	render() {
		for(let key in this.component) {
			this[key].trigger('render');
		}
	}
}
