//
// Release
//

'use strict';

module.exports = function(gulp, config, $) {

    var fs = require('fs'),
        exec = require('sync-exec'),
        prompt = require('prompt'),
        semver = require('semver'),
        runSequence = require('run-sequence'),
        packageFile = '../package.json',
        pack = require(packageFile);


    console.log("flag:", config.argv.nointeraction);

    // gets version from user and build-count from git repo
    gulp.task('getVersion', function(callback) {

        // make sure repo is up to date
        if(exec('git status').stdout.indexOf('up-to-date') == -1) {
            $.util.log($.util.colors.bgRed (' Please Make sure your local repo is up to date before running Gulp Release '));
            process.exit(1);
        }

        config.semver.commitCount = parseInt(exec("git rev-list HEAD | wc -l | tr -d ' '").stdout); // trap errors?

        // setup temp folder
        exec('rm -rf .tmp');
        config.styles.dest = '.tmp/styles';
        config.scripts.dest = '.tmp/scripts';
        config.iconfont.dest = '.tmp/iconfont';
        config.iconfont.svg.dest = '.tmp/img/svg/icon';

        $.util.log( "Input version number, press enter for current version " + $.util.colors.bold("(" + pack.version + "), or q to quit:" ));

        getVerNum();

        function getVerNum() {


            if(config.argv.nointeraction) {
                config.semver.version = pack.version;
                config.semver.newBuild = false;
                return callback();
            }
            else prompt.start();

            // trap cntl-c inside here, if user cancels-out it throws an error
            prompt.get(['version'], function ( err, result ) {

                if(result.version == '') { // user left blank, use same version
                    config.semver.version = pack.version;
                    config.semver.newBuild = false;
                    return callback();
                }
                else { // new version

                    if(semver.valid(result.version) && semver.gte(result.version, pack.version)) {
                        config.semver.version = result.version;
                        config.semver.newBuild = true;
                        return callback();
                    }
                    else getVerNum();
                }
            });
        };
    });

    gulp.task('update', function(callback) {

        if(config.semver.quit) return callback(); // user typed quit, don't run update tasks

        if(!config.semver.newBuild) {

            //config.styles.dest = 'dist/styles';
            //config.scripts.dest = 'dist/scripts';
            //config.iconfont.dest = 'dist/iconfont';
            //config.iconfont.svg.dest = 'dist/img/svg/icon';

            return callback();
        }
        // update package.json with new version #
        pack.version = config.semver.version;
        fs.writeFileSync('./package.json', JSON.stringify(pack, null, 2));

        // update VERSION.txt file
        var contents =
            'Version:' + config.semver.version + '\n' +
            'Build:' + config.semver.commitCount;
        fs.writeFileSync('./VERSION' , contents);


        return callback();
    });

    // --- build here ---

    gulp.task('postBuild', function(callback) {

        if(config.semver.quit) return callback(); // user typed quit, don't run post build tasks

        exec('rm -rf dist/*');      // clean the dist folder
        exec('cp -r .tmp/* dist/'); // copy all files to /release

        exec( 'git add -A dist/*' );
        exec( 'git add -A source/*' );
        exec( 'git add -A VERSION' );

        exec('git commit -m "Release for v' + config.semver.version + '"' );
        
        // NOTE: Keep Auto-Push Disabled! People should have to manually
        // push their releases, so they can confirm they worked properly.
        // exec('git push');

        if(!config.argv.notag) {

            // look for tag in repo, if pre-existing, delete
            var exists = exec( 'git tag -l | grep ' + config.semver.version ).stdout;
            if(exists.length) {

                exec('git tag -d ' + config.semver.version);
                exec('git push origin :refs/tags/' + config.semver.version);
            }
            exec('git tag ' + config.semver.version);

            // NOTE: Keep Auto-Push Disabled! People should have to manually
            // push their releases, so they can confirm they worked properly.
            // exec('git push --tags');

        }

        console.log( "\n======================================================================" );
        console.log( "   Release commit & tag created. Be sure to push both to your remote.\n");
        console.log( "   $ git push origin && git push --tags\n");
        console.log( "   NOTE: You can undo your commit by doing: ");
        console.log( "   $ git reset --hard origin/{current/branch} ")
        console.log( "========================================================================\n" );

        return callback();
    });

    gulp.task('release', function() {

        runSequence(['getVersion'], ['update'], ['build'], ['postBuild']);
    });
};
