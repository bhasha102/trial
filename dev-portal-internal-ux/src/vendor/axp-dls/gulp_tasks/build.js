//
// Build
//

'use strict';

module.exports = function(gulp, config, $) {

	gulp.task('build', function(cb) {

		return require('run-sequence')(
			['clean'],
			['iconfont'],
			['iconfont:svg'],
			['styles'],
			['scripts'],
			function() {
				return cb();
			}
		);
	});

	gulp.task('default', ['build']);
};
