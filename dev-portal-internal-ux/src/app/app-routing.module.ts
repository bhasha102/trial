import {Routes,RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import { ProductListComponent} from './product/product-list/product-list.component';
import { ProductDetailComponent} from './product/product-details/product-detail.component';
import { ManageUserComponent} from './admin/platform/manageuser.component';
import { AddPlatformComponent} from './admin/platform/add-platform/addplatform.component';
import { EditPlatformComponent} from './admin/platform/edit-platform/editplatform.component';
import { AdminProductComponent} from './admin/product/add-product/addproduct.component';
import { DeletePlatformComponent} from './admin/platform/delete-platform/delete-platform.component';
import { DeleteProductComponent} from './admin/product/delete-product/delete-product.component';
import { AdminEditProductComponent} from './admin/product/edit-product/editproduct.component';
//-------
import { FaqComponent } from './faq/faq.component';
import { EauthComponent } from './eauth/eauth.component';
import { PlatformAdminComponent } from './platform-admin/platform-admin.component';
import { PublicProductComponent } from './product/product-list/public-product.component';
import { UserProductComponent } from './product/product-list/user-product.component';
import { AdminComponent } from './admin/admin.component';


const APP_ROUTE: Routes = [

   {path: '', redirectTo: 'products', pathMatch : 'full'},
    {path: 'products', component : ProductListComponent , children: [
        { path: '', component: PublicProductComponent, outlet: 'product-nav' , redirectTo : 'publicProduct'},
        { path: 'publicProduct', component: PublicProductComponent, outlet: 'product-nav'},
        { path: 'userProduct', component: UserProductComponent, outlet: 'product-nav' },
    ]},
    {path: 'productdetails/:id/:name', component : ProductDetailComponent},
    {path: 'faq', component: FaqComponent},
    {path: 'eauth', component: EauthComponent},
    {path: 'platformAdmin', component: PlatformAdminComponent},
    {path: 'admin', component: AdminComponent, children: [
        { path: '', component: ManageUserComponent, outlet: 'admin-nav', redirectTo : 'platform'},
        { path: 'platform', component: ManageUserComponent, outlet: 'admin-nav' },    
        { path: 'addPlatform', component: AddPlatformComponent, outlet: 'admin-nav' },
        { path: 'deletePlatform', component: DeletePlatformComponent, outlet: 'admin-nav' },
        { path: 'deleteProduct', component: DeleteProductComponent, outlet: 'admin-nav' },
        { path: 'editPlatform', component: EditPlatformComponent, outlet: 'admin-nav' },
        { path: 'adminaddproduct', component: AdminProductComponent, outlet: 'admin-nav' },
        { path: 'admineditproduct', component: AdminEditProductComponent, outlet: 'admin-nav' }
    //------
    ]
  },
]

export const routing = RouterModule.forRoot(APP_ROUTE);



