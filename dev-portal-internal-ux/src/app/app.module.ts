import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Title }     from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { FaqComponent } from './faq/faq.component';
import { AmexHeaderComponent } from './arch/amex-header/amex-header.component';
import { AmexFooterComponent } from './arch/amex-footer/amex-footer.component';
import { OrderByPipe } from './arch/amex-header/order-by.pipe';

import { AdminComponent } from './admin/admin.component';
import { PlatformAdminComponent } from './platform-admin/platform-admin.component';
import { ProductComponent } from './product/product.component';
import { ProductListComponent} from './product/product-list/product-list.component';
import { ProductService} from './product/product-list/product-list.service';
import { ProductDetailComponent} from './product/product-details/product-detail.component';
import { ProductDetailService} from './product/product-details/product-detail.service';
import { ViewUserAppsComponent } from './view-user-apps/view-user-apps.component';
import { ProdApiDocComponent } from './prod-api-doc/prod-api-doc.component';
import { CreateAppComponent } from './create-app/create-app.component';
import { routing } from './app-routing.module';

import { FaqService} from './faq/faq.service';
import { FilterPipe} from './faq/faqfilter.pipe'
import { EauthComponent } from './eauth/eauth.component';
import { ManageUserService} from './admin/platform/manageuser.service';
import { ManageUserComponent} from './admin/platform/manageuser.component';
import { AddPlatformComponent} from './admin/platform/add-platform/addplatform.component';
import { AddPlatformService} from './admin/platform/add-platform/addplatform.service';
import { EditPlatformComponent} from './admin/platform/edit-platform/editplatform.component';
import { EditPlatformService} from './admin/platform/edit-platform/editplatform.service';
import { DeletePlatformComponent} from './admin/platform/delete-platform/delete-platform.component';
import { DeletePlatformService} from './admin/platform/delete-platform/delete-platform.service';
import { DeleteProductComponent} from './admin/product/delete-product/delete-product.component';
import { DeleteProductService} from './admin/product/delete-product/delete-product.service';
import { AdminProductComponent} from './admin/product/add-product/addproduct.component';
import { AdminAddProductService} from './admin/product/add-product/addproduct.service';
import { AdminEditProductComponent} from './admin/product/edit-product/editproduct.component';
import { AdminEditProductService} from './admin/product/edit-product/editproduct.service';
//---------
import { SanitizeHtml } from './product/product-details/sanitizehtml.pipe';
import { KeysPipe } from './product/product-details/iterateMap.pipe';
import { Ng2PageScrollModule} from 'ng2-page-scroll';
import { PublicProductComponent } from './product/product-list/public-product.component';
import { UserProductComponent } from './product/product-list/user-product.component';

@NgModule({
  declarations: [
    AppComponent,
    FaqComponent,
    AmexHeaderComponent,
    AmexFooterComponent,
    AdminComponent,
    PlatformAdminComponent,
    ProductComponent,
    ViewUserAppsComponent,
    ProdApiDocComponent,
    CreateAppComponent,
    ProductListComponent,
    ProductDetailComponent,
    FilterPipe,
    EauthComponent,
    ManageUserComponent, 
    AddPlatformComponent,
    EditPlatformComponent,
    AdminProductComponent,
    DeletePlatformComponent,
    DeleteProductComponent,
    AdminEditProductComponent,
    //---------
    SanitizeHtml,
    KeysPipe,
    PublicProductComponent,
    UserProductComponent,
    OrderByPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule,
    Ng2PageScrollModule.forRoot()

  ],
  providers: [Title, ProductDetailService, ProductService, FaqService, ManageUserService, AddPlatformService, EditPlatformService, AdminAddProductService, DeletePlatformService, DeleteProductService, AdminEditProductService], 
  bootstrap: [AppComponent]
})
export class AppModule { }
