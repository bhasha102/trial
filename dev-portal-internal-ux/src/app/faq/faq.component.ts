import { Component, OnInit  } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {FaqService} from './faq.service';
import {FAQ} from './faq';
import {Observable} from 'rxjs/Rx';
import {FilterPipe} from './faqfilter.pipe'

@Component({
    selector: 'faq',
        templateUrl:'./faq.component.html',
        styleUrls :  ['./faq.component.less']
})

export class FaqComponent implements OnInit{
 pageTitle:string ;
 question:FAQ[] ;
 errorMessage:string;

        public constructor(private titleService: Title, private faqService: FaqService ) { 
            this.setTitle();
                 }
        public getTitle()
        {
            this.pageTitle = this.titleService.getTitle();
        }
         public setTitle() {
             this.getTitle();       
            this.titleService.setTitle( this.pageTitle +" Faq" );
    }
    ngOnInit(){
        this.getQuestion();
    }

    getQuestion()
    {
        this.faqService.getQuestions().subscribe(
                        question => this.question  = question,
                        error =>  {console.log(error)});
    }
}