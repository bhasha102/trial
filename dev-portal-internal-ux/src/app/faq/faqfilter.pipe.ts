import { Pipe, PipeTransform } from '@angular/core';
import {FAQ} from './faq';

@Pipe({
    name: 'filter',
})
export class FilterPipe implements PipeTransform {

    transform(question: FAQ[], nameFilter: string):any {
        console.log(question );
         if (nameFilter === undefined ) 
         {
               console.log("nameFilter : "+ nameFilter);
               return question;
         }
       
          return question.filter(function(qt){
              if (qt.questn.indexOf(nameFilter) !== -1)
              return qt.questn.toLowerCase().includes(nameFilter.toLowerCase());
              if (qt.answer.indexOf(nameFilter) !== -1)
              return qt.answer.toLowerCase().includes(nameFilter.toLowerCase()); 
          })
    }
}