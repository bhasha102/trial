import { Component, EventEmitter, Input, OnChanges, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { ProductService} from './product-list.service';
import { Router } from '@angular/router';
import { Product} from '../product';

import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'product-container',
    templateUrl: './user-product.component.html',
    styleUrls : ['./user-product.component.less']
})
export class UserProductComponent implements OnInit {
        pageTitle:string;
        productsDetails:Product[];
        errorMessage:string;
        sortVal : string;
        sortType:string = 'Asc';

        constructor(private titleService: Title, private _productService : ProductService, private router:Router ) {           
        }
       

    ngOnInit(){
        this.getProducts();
    }

    getProducts()
    {
        this._productService.getProductList().subscribe(
                        productsDetails => this.productsDetails = productsDetails,
                        error =>  {console.log(error)});
                        
    }
    sort(type,key){
        if(type == 'Asc')
        {
            this.sortVal = key;
            this.sortType = 'Desc';
        }else if(type == 'Desc')
        {
            this.sortVal = "-"+key;
            this.sortType = 'Asc';
        }
    }
    productDetail(item:Product){
        this.router.navigate(['/productdetails', item.productLink, item.product_name]);
    }
}