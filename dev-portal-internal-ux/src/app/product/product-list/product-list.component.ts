import { Component } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'product-container',
    templateUrl: './product-list.component.html',
    styleUrls : ['./products-list.component.less']
})
export class ProductListComponent {
        pageTitle:string;
        errorMessage:string;

        constructor(private titleService: Title, private router:Router ) { 
            this.setTitle();
        }
        getTitle(){
            this.pageTitle = this.titleService.getTitle();
        }
        setTitle() {
            this.getTitle();       
            this.titleService.setTitle( this.pageTitle +" Product" );
    }

}