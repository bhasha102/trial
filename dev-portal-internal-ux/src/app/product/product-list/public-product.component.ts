import { Component, EventEmitter, Input, OnChanges, OnInit } from '@angular/core';
import { ProductService} from './product-list.service';
import { Router } from '@angular/router';
import { Product} from '../product';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'product-container',
    templateUrl: './public-product.component.html',
    styleUrls : ['./public-product.component.less']
})
export class PublicProductComponent implements OnInit {
        
        productsDetails:Product[];
        errorMessage:string;
        newProductData : Array<any>;
        sortVal : string;
        sortType : string = 'Asc';
       // publicProductHeader = ['Platform Name'];
       // tableKey = ['platformName'];

       // productsData= [{'platformName':"ancdasdfsadf"}]
        //publicProductHeader = ['Platform Name', 'Product Name', 'Product Description', 'Date Created', 'Product Documentation'];
      //  tableKey = ['platformName', 'product_name', 'product_desc', 'dateCreated', 'productLink'];
        constructor( private _productService : ProductService, private router:Router ) {           
        }
       

    ngOnInit(){
        this.getProducts();
        //this.changeProductDataFormat();
    }

    getProducts()
    {
        this._productService.getProductList().subscribe(
                        productsDetails => this.productsDetails = productsDetails,
                        error =>  {console.log(error)}
        );
                        
    }

    productDetail(item:Product){
        console.log(item);
        this.router.navigate(['/productdetails', item.productLink, item.product_name]);
    }

    sort(type,key){
        if(type == 'Asc')
        {
            this.sortVal = key;
            this.sortType = 'Desc';
        }else if(type == 'Desc')
        {
            this.sortVal = "-"+key;
            this.sortType = 'Asc';
        }
    }
   /* changeProductDataFormat(prodetails)
    {
              
        this.newProductData = [];
        for(let i =0;i < prodetails.length;i++)
        {
            let tempObj = new Object;
            tempObj['platformName'] = prodetails[i].platformName;
            tempObj['product_name']= prodetails[i].product_name;
            tempObj['product_desc']= prodetails[i].product_desc;
            tempObj['dateCreated']= prodetails[i].dateCreated;
            tempObj['productLink']= "<a class='documentationbutton' (click)=productDetail("+prodetails[i]+");>Documentation</a>";//this.productsDetails[i].dateCreated;
            this.newProductData.push(tempObj);    
        }

        //<a class="documentationbutton" (click)=productDetail(item);>Documentation</a>
    }*/
    
}