export interface Product{
    platformName : string,
    product_name: string,
    product_desc: string,
    dateCreated: Date,
    productLink : number
}