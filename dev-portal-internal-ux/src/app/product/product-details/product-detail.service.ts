import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Product} from '../product';
import {JsonpModule} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProductDetailService{
  private baseUrl: string = "./app/product/product-details/productDetails.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
 
  constructor(private http : Http){
  }

  // get product details
    getProductDetails (body: Object): Observable<JSON> {
        let bodyString = JSON.stringify({body}); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
       // console.log(bodyString);
        return this.http.get(this.baseUrl)
        // ...and calling .json() on the response to return data
          .map((res:Response) => res.json())
          .catch((error:any) => Observable.throw('Server error'))

        /*return this.http.post(this.baseUrl, bodyString, options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
                         */
    }

}