import { Component, EventEmitter, Input, OnChanges, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { Observable } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
//import { ProductStorage } from '../product-storage.service';
import { Product } from '../product';
import { ProductDetailService } from './product-detail.service';
import { SanitizeHtml } from './sanitizehtml.pipe';
import { KeysPipe } from './iterateMap.pipe';

@Component({
    selector: 'product-container',
    templateUrl: './product-detail.component.html',
    styleUrls : ['./product-detail.component.less', '../../../assets/axp-dev-portal/css/common.css']
})

export class ProductDetailComponent implements OnInit {
        sub :any;
        documentId : any;
        documentName : string;
        productsDetails:any[];
        responseObj :JSON;
        responseTxt:string;
        highlightedDiv : number;

        leftNav = {"apiDocSection1":"Express Checkout","apiDocSection2":"Getting Started","apiDocSection3":"Javascript SDK","apiDocSection4":"iOS SDK","apiDocSection5":"Android SDK","apiDocSection6":"API Overview","apiDocSection7":"API Security","apiDocSection8":"API Environments","apiDocSection9":"Support"};

        constructor(private route: ActivatedRoute , private _productDetailService : ProductDetailService){
        }

     ngOnInit(){
         this.sub = this.route.params.subscribe(params => {
            this.documentId = params['id'];
            this.documentName = params['name'];      
        });
        this.getDetails();
        //window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );      
     }

     getDetails(){
        this._productDetailService.getProductDetails({id: this.documentId, name: this.documentName}).subscribe(
            response => this.responseObj = response,
            error => {console.log(error)}
        );
     }

     focusFunction(){
        if(window['CKEDITOR'].instances['prodAPIDoc'] == undefined)
        {
            window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );
           
        }
       // console.log("in focus");
       // console.log(window['CKEDITOR'].instances['prodAPIDoc']);
        //window['CKEDITOR']['inline']( 'prodAPIDoc',{customConfig : '../../../assets/axp-dev-portal/ckeditor/config.js'} );
     }

     isActiveClass(index){
         if(index == 0)
         {
             return "active";
         }
         return "";
     }

     addActiveClass(newValue: number){
        if (this.highlightedDiv === newValue) {
            this.highlightedDiv = -1;
        }
        else {
            this.highlightedDiv = newValue;
        }
  }
     focusOutFunction()
     {
         //console.log("out focus");
         let data = window['CKEDITOR'].instances['prodAPIDoc'].getData();
         //console.log(data);
         window['CKEDITOR'].instances['prodAPIDoc'].destroy(true);
         //console.log(window['CKEDITOR'].instances['prodAPIDoc']);
        
     }
}
