import { Component } from '@angular/core';

@Component({
    selector: 'amex-app',
    templateUrl: './app.component.html',
    styleUrls : ['../less/main.less']
})
export class AppComponent {
    pageTitle:string = "Home";

}
