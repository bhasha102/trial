import { Injectable } from '@angular/core';
import {AdminProduct} from './adminproduct';
import {AdminProductSelect} from './adminproductselect';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminEditProductService{
private baseUrl: string = "./app/admin/product/edit-product/adminproduct.json";
private baseUrl1: string = "./app/admin/product/edit-product/adminproductselect.json";

constructor(private http : Http){
  }
  getAdminProduct():Observable<AdminProduct[]>{
    return this.http.get(this.baseUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }
   getAdminProductList():Observable<AdminProductSelect[]>{
    return this.http.get(this.baseUrl1)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }


  // Add a new product
    getProductDetails (body: FormData): Observable<any[]> {
        let bodyString = body; // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'multipart/form-data' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.baseUrl, body, options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }  
   
}