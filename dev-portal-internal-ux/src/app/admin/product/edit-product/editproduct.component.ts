import { Component, OnInit  } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Observable} from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { AdminProduct} from './adminproduct';
import { AdminEditProductService} from './editproduct.service';
import { Platform } from '../../platform/platform';
import { EditPlatformService } from '../../platform/edit-platform/editplatform.service';
import {AdminProductSelect} from './adminproductselect';
@Component({
        selector: 'editProduct',
        templateUrl:'./editproduct.component.html',
        styleUrls :  ['./editproduct.component.less']
})

export class AdminEditProductComponent implements OnInit {
   public myForm: FormGroup; // our model driven form
   public submitted: boolean; // keep track on whether form is submitted
   public events: any[] = []; // use later to display form changes
   errorMessage:string;
   editPRO : AdminProduct[];
   showModal : boolean = false;
   loaded: boolean = false;
   imageLoaded: boolean = false;
   imageSrc: string = '';
   file : Blob;
   returnVal : any;
   textValue =[];
   platformDetails : Platform[];
   showtext : boolean =true;
   showmultiple : boolean;
   selectOption : any;
   select : any;
   listPRO : AdminProductSelect[];
   categoryUpdate : string = "PRIVATE";
   fileUpdated:string;
   apProdList : Array<string> = [];
   isApigeeProd :string = "nonapigee";
   updatedPlatformId :string;
   selectPlatform : string;
   selectCategory:string='nonapigee';
     updatedMsg :boolean = false;
   category = [{name:"Non-Apigee Product",code:"nonapigee"},{name:"Link Apigee Product",code:"realtime"}];
  constructor(private _fb: FormBuilder, private _editproductService : AdminEditProductService, private _addplatformService : EditPlatformService) { } 

    ngOnInit(){
            this.myForm = this._fb.group({
            prodDescription : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            prodName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            editProduct : ['',[<any>Validators.required]],
            prodLocation : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload : [''],
            chooseCategory : [''],
            platform :[''],
           // prodCategory:['']
        });
      
        this.getDetails();
       // this.selectOption = this.addPl[0];
      //  console.log(this.selectOption)
        this.select ="Non-Apigee Product";
 
    }

    isApigeeProduct(apigeeProd){
            console.log(apigeeProd)
            if("realtime" ==  apigeeProd)
            {
                  this.selectCategory = this.category[1].code;
                  // this.selectCategory = "realtime";
                   this.showmultiple = true;
                   this.showtext = false;
            }
            else
            {
                   this.selectCategory = this.category[0].code;
                   // this.selectCategory = "nonapigee";
                    this.showtext = true;
                    this.showmultiple = false;
            }
    }

    getDetails()
    {
               this._addplatformService.getPlatformDetails().subscribe(
                        UserDetails => this.platformDetails = UserDetails,
                        error =>  {console.log(error)}
                          );

        this._editproductService.getAdminProduct().subscribe(
                        adminPRO => this.editPRO  = adminPRO,
                        error =>  {console.log(error)});
                        
       this._editproductService. getAdminProductList().subscribe(
                        adminPRO => this.listPRO  = adminPRO,
                        error =>  {console.log(error)});
                        
    }
     onChange(orgId)
     {
            // Need to pass orgId in service
            this._editproductService.getAdminProduct().subscribe(
                        adminPRO => this.editPRO  = adminPRO,
                        error =>  {console.log(error)},
                        () => {this.updateProductDetails()}         
            );
           // this.showMessage =false;

     //   this._addplatformService.getUserDetails(plat).subscribe(
    //        response => this.responseObj = response,
     //       error => {console.log(error)}
     //   );
     }

     updateProductDetails()
     {
         //this.myForm.patchValue({'city':this.platformDetails[0].city},{'orgName': this.platformDetails[0].orgName});
         this.myForm.controls['prodName'].patchValue(this.editPRO[0].prodName);
         this.myForm.controls['prodDescription'].patchValue(this.editPRO[0].prodDs);
         this.myForm.controls['prodLocation'].patchValue(this.editPRO[0].prodLocTx);
        // this.myForm.controls['prodCategory'].patchValue(this.editPRO[0].pvtCtgyId);
         if(this.editPRO[0].apProdList[0] === 'NonApigee Product')
         {
                this.showtext = true;
                this.showmultiple = false;
                this.isApigeeProd = "nonapigee";
                //this.selectCategory = "nonapigee";
                this.selectCategory = this.category[0].code;
         }
         else{
                this.showmultiple = true;
                this.showtext = false; 
                this.apProdList = this.editPRO[0].apProdList;
                this.isApigeeProd = "realtime";
                this.selectCategory = this.category[1].code;//"realtime";
                console.log("thirdtime" +this.selectCategory);
         }
        this.categoryUpdate = this.editPRO[0].pvtCtgyId;
        this.fileUpdated = this.editPRO[0].prodName+".json";
        this.selectPlatform = this.editPRO[0].orgId.toString();
     }

    handleInputChange(e){
         console.log("change");
         this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];        
         console.log(this.file);
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

    save(formValue, isValid)
    {
        // var input = new FormData();
        // input.append( 'prodDescription',formValue.prodDescription);
        // input.append( 'prodName',formValue.prodName);
        // input.append( 'adminProduct',formValue.adminProduct);
        // input.append( 'prodLocation',formValue.prodLocation);
        // input.append( 'selectCategory',formValue.selectCategory);
        formValue.uploadFile = this.file;
        console.log(formValue.uploadFile);
        console.log(formValue);
        this._editproductService.getProductDetails(formValue).subscribe(
                adminPl => this.returnVal  = adminPl,
                error =>  {console.log(error)},
                () => {console.log(this.returnVal)})
        this.updatedMsg = true;
        //this.myForm.reset();
        this.submitted = true;
    }
     removeUpdateMsg()
    {
        this.updatedMsg = false;
    }
   
    displaySelect()
    {
     
    }
}
