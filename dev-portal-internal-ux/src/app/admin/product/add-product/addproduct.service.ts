import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminAddProductService{
private baseUrl2: string = "./app/admin/product/add-product/admin-product-apigee.json";
constructor(private http : Http){
  }
     getApigeeList():Observable<Object[]>{
    return this.http.get(this.baseUrl2)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }

  // Add a new product
    // getProductDetails (body: FormData): Observable<any[]> {
    //     let bodyString = body; // Stringify payload
    //     let headers      = new Headers({ 'Content-Type': 'multipart/form-data' }); // ... Set content type to JSON
    //     let options       = new RequestOptions({ headers: headers }); // Create a request option

    //     return this.http.post(this.baseUrl, body, options) // ...using post request
    //                      .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    //                      .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    // }  
      
}