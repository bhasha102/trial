import { Component, OnInit  } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Observable} from 'rxjs/Rx';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { AdminAddProductService} from './addproduct.service';
import { EditPlatformService } from '../../platform/edit-platform/editplatform.service';
import { Platform } from '../../platform/platform';
import { ApigeeProduct} from './apigee-product';

@Component({
        selector: 'adminaddproduct',
        templateUrl:'./addproduct.component.html',
        styleUrls :  ['./addproduct.component.less']
})

export class AdminProductComponent implements OnInit {
   public myForm: FormGroup; // our model driven form
   public submitted: boolean; // keep track on whether form is submitted
   public events: any[] = []; // use later to display form changes
   errorMessage:string;
   showModal : boolean = false;
   loaded: boolean = false;
   imageLoaded: boolean = false;
   imageSrc: string = '';
   file : Blob;
   returnVal : any;
   apigeeList : Object[];
   textValue =[];
   isApigeeProd :string = "nonapigee";
   showtext : boolean = true;
   showmultiple : boolean;
   selectOption : any;
   select : any;
   platformDetails : Platform[];
   newApigeeList : Array<ApigeeProduct>;
   selectCategory:string='nonapigee';
   selectcategory = [{name:"Non-Apigee Product",code:"nonapigee"},{name:"Link Apigee Product",code:"realtime"}];
   updatedMsg :boolean = false;
  constructor(private _fb: FormBuilder, private _addproductService : AdminAddProductService, private _addplatformService : EditPlatformService) { } 

    ngOnInit(){
            this.myForm = this._fb.group({
            prodDescription : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            prodName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            adminProduct : [''],
            prodLocation : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            fileUpload : [''],
            chooseCategory : ['']
         
 
        });
        this.platformDetails=[];
        this.getAddPlatform();
        this.select ="Non-Apigee Product";
            
    }
    isApigeeProduct(apigeeProd){
             console.log("isApigeeProduct func : "+apigeeProd);
            if("realtime" ==  apigeeProd)
            {
                    console.log("before json call isApigeeProduct");
                    this._addproductService.getApigeeList().subscribe(
                        apigeeList => this.apigeeList  = apigeeList,
                        error =>  {console.log(error)},
                         () =>  {this.convertApigeeList(this.apigeeList)} //this.selectOption = this.platformDetails[2],console.log(this.selectOption)
                        );
                  console.log("after json call isApigeeProduct");
            }
            else
            {
                this.selectCategory = "nonapigee";
                    console.log(" isApigeeProduct nonapigee");
                    this.showModal = false;
                    this.showtext = true;
                    this.showmultiple = false;
            }
    }
 
   removeUpdateMsg()
    {
        this.updatedMsg = false;
    }

    getAddPlatform()
    {
         this._addplatformService.getPlatformDetails().subscribe(
                        UserDetails => this.platformDetails = UserDetails,
                        error =>  {console.log(error)}
                          );
    }
    handleInputChange(e){
        // console.log("change");
         this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0]; 
               
        //  console.log(this.file['name']);
        //  let ext = this.file['name'].split('.');
        
        //  if(ext != 'json')
        //  {
        //      console.log(ext[1]);
        //      //this.myForm.controls['fileUpload'].valid = false;
        // }
    }

    convertApigeeList(list)
    {
        console.log("convertApigeeList");
        this.newApigeeList= [];
        for (var key in list) {
            var value = list[key];
           
           let tempObj:ApigeeProduct ={name:"", author:"", state:false};
            tempObj['name'] = key; 
            tempObj['author'] = value; 
            tempObj['state'] = false;  
            this.newApigeeList.push(tempObj);
        }
        this.selectCategory = "realtime";
        this.showModal =true;
        this.showmultiple = true;
        this.showtext = false;
    }
     save(formValue, isValid)
     {
    //      var input = new FormData();
    //     input.append( 'prodDescription',formValue.prodDescription);
    //     input.append( 'prodName',formValue.prodName);
    //     input.append( 'adminProduct',formValue.adminProduct);
    //     input.append( 'prodLocation',formValue.prodLocation);
    //     input.append( 'selectCategory',formValue.selectCategory);
    //     formValue.uploadFile = this.file;
    //     console.log(formValue.uploadFile);
    //     console.log(formValue);
    //     this._addproductService.getProductDetails(formValue).subscribe(
    //             adminPl => this.returnVal  = adminPl,
    //             error =>  {console.log(error)},
    //             () => {console.log(this.returnVal)})
          this.updatedMsg = true;
            this.myForm.reset();
        this.submitted = true;
    }

   
    displaySelect()
    {

        let j=0;
        this.textValue = [];
        for(let i =0;i< this.newApigeeList.length;i++)
        {
                if(this.newApigeeList[i].state)
                {
                    console.log(this.newApigeeList[i].state)
                        this.textValue[j] = this.newApigeeList[i].name;
                        
                        console.log(this.textValue[j]);
                        j++;
                }
        }
        
        if(j === 0)
        {
                this.isApigeeProd = "nonapigee";
                  this.selectCategory = "nonapigee";
                this.showtext = true;
                this.showmultiple = false;
        }
        else
        {
                this.isApigeeProd = "realtime";
                this.selectCategory = "realtime";
                this.showtext = false;
                this.showmultiple = true;
        }
        this.showModal =false;
    }
    close()
    {
            this.isApigeeProd = "nonapigee";
            this.selectCategory = "nonapigee";
            this.showtext = true;
            this.showmultiple = false;
            this.showModal =false;
            console.log(this.isApigeeProd);
    }

    closeModal(event){
        console.log(event.target.className);
        if(event.target.className == "modal"){
            this.showModal =false;
            this.showtext = true;
            this.showmultiple = false;
            this.selectCategory = "nonapigee";
        }
    }
}
    
