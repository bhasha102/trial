import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { DeleteProductService } from './delete-product.service';
import { ProdList} from './delete-product';
@Component({
  
    selector: 'deleteproduct',
    templateUrl: './delete-product.component.html',
    styleUrls: ['./delete-product.component.less']
})
export class DeleteProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
     errorMessage:string;
     product:ProdList[];
     updatedMsg : boolean = false;
     errorMsg : boolean = false;
     constructor(private _fb: FormBuilder, private _delproductService : DeleteProductService) { }
     ngOnInit() {
        this.myForm = this._fb.group({
             deleteProduct: ['']
               });
      this.getProductsList();
     }

   getProductsList()
   {
        this._delproductService.getProducts().subscribe(
                    productDetails => this.product = productDetails,
                    error =>  {console.log(error)});
         console.log(this.product);
    }
    onChange(){
        console.log(this.updatedMsg);
        this.updatedMsg = false;
          this.errorMsg = false;
    }

    deleteRowForm(plat){
          if (plat === "")
        { 
            this.errorMsg = true;
            this.updatedMsg = false;
        }
        else
        { 
           console.log(plat);
           this.updatedMsg = true;
           this.errorMsg = false;
           this.product = this.product.filter(function(qt){
              if (qt.id !== plat)
              return qt;
          })
        }
    }
}