import { Injectable } from '@angular/core';
import { ProdList} from './delete-product';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DeleteProductService{
private baseUrl: string ="./app/admin/product/delete-product/delete-product.json";
constructor(private http : Http){
  }
  
  getProducts():Observable<ProdList[]>{
       
    return this.http.get(this.baseUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     
  }
    
}