
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Country } from '../country';
import {JsonpModule} from '@angular/http';
import { States } from '../states';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AddPlatformService{
  private baseUrl: string = "./app/admin/platform/add-platform/countries.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  private baseUrl1: string = "./app/admin/platform/add-platform/states.json";
  constructor(private http : Http){
  }
  getCountryList():Observable<Country[]>{
	const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

   getStatesList():Observable<States[]>{
	const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl1)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

}