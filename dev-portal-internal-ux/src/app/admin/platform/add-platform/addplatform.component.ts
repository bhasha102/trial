import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { Platform } from '../platform';
import { AddPlatformService } from './addplatform.service';
import { Country } from '../country';
import { States } from '../states';

@Component({
  
    selector: 'addPlatform',
    templateUrl: './addplatform.component.html',
    styleUrls: ['./addplatform.component.less']
})
export class AddPlatformComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    country : Array<Country>;
    states :  Array<States>;
    //selectedCountry : any = null;
    errorMessage:string;
    selectCountry:string='US';
    selectStates:string='001';
    updatedMsg :boolean = false;
    constructor(private _fb: FormBuilder, private _addplatformService : AddPlatformService) { } // form builder simplify form initialization

    ngOnInit() {
        this.myForm = this._fb.group({
            platformName : ['',[<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            phoneNumber : ['', [<any>Validators.required]],
            addressline1: ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            addressline2: [''],
            city : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            zip : ['', [<any>Validators.required,Validators.minLength(5),Validators.maxLength(5)]],
            country : [''],
            states: ['']
        });
        
        this.getCountryList();
        this.getStatesList('US');
        
    }
    

    // loadModules(){
    //     this.selectedCountry = this.country[0];
    //     //load you modules set selectedModule to the on with the
    //     //id of modInst.modID[0]._id you can either loop or use .filter to find it.
    // this.selectedCountry = this.states[0];
    // }
    getCountryList()
    {
        this._addplatformService.getCountryList().subscribe(
             country => this.country  = country,
             error =>  {console.log(error)}
             );

    }
    getStatesList(countryId)
    {
        console.log(countryId);
        this._addplatformService.getStatesList().subscribe(
             states => this.states  = states,
             error =>  {console.log(error)},
             ()=> this.selectStates = '001'
             );
    }

    save(model: any, isValid: boolean) {
       
        this.updatedMsg = true;
        // check if model is valid
        // if valid, call API to save customer
        console.log(model, isValid);
        this.myForm.reset();
        this.submitted = true; // set form submit to true
    }
    
    removeUpdateMsg()
    {
        this.updatedMsg = false;
    }
}