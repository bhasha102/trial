import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { DeletePlatformService } from './delete-platform.service';
import { PList} from './delete-platform';
@Component({
  
    selector: 'deleteplatform',
    templateUrl: './delete-platform.component.html',
    styleUrls: ['./delete-platform.component.less']
})
export class DeletePlatformComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
     errorMessage:string;
     platform:PList[];
     updatedMsg : boolean = false;
     errorMsg : boolean = false;
     constructor(private _fb: FormBuilder, private _delplatformService : DeletePlatformService) { }
     ngOnInit() {
        this.myForm = this._fb.group({
             deleteplatform: ['']
               });
      this.getPlatformsList();
     }

           getPlatformsList()
    {
        this._delplatformService.getPlatforms().subscribe(
                        platformDetails => this.platform = platformDetails,
                        error =>  {console.log(error)});
       console.log(this.platform);
    }
   onChange(){
        console.log(this.updatedMsg);
        this.updatedMsg = false;
        this.errorMsg = false;
    }
    deleteRowForm(plat){
        console.log(plat);
        if (plat === "")
        { 
            this.errorMsg = true;
            this.updatedMsg = false;
        }
        else
            { 
                this.updatedMsg = true;
                this.errorMsg = false;
                this.platform = this.platform.filter(function(qt){
                if (qt.id !== plat)
                return qt;
                })
            }
    }
}