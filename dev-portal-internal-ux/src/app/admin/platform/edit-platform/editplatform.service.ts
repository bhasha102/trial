
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Country } from '../country';
import {JsonpModule} from '@angular/http';
import { States } from '../states';
import { Platform } from '../platform';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EditPlatformService{
  private baseUrl: string = "./app/admin/platform/add-platform/countries.json";//'http://localhost:8085/dev-portal-intra/dummyProducts';
  private baseUrl2: string ="./app/admin/platform/admin.json";
  constructor(private http : Http){
  }
  getCountryList():Observable<Country[]>{
	const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

   getStatesList():Observable<States[]>{
	const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }

  getPlatformDetails():Observable<Platform[]>{
    const headers = new Headers();
  	headers.append('Access-Control-Allow-Headers', 'Content-Type');
  	headers.append('Access-Control-Allow-Methods', 'GET');
  	headers.append('Access-Control-Allow-Origin', '*');
      return this.http.get(this.baseUrl2)
      // ...and calling .json() on the response to return data
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw('Server error'))
  }

  saveEditPlatform (body: FormData): Observable<string> {
        let bodyString = body; // Stringify payload
        //let headers      = new Headers({ 'Content-Type': 'multipart/form-data' }); // ... Set content type to JSON
        //let options       = new RequestOptions({ headers: headers }); // Create a request option

       // return this.http.post(this.baseUrl, body, options) // ...using post request
        return this.http.get(this.baseUrl2)
                         .map((res:Response) => "success") // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw('Server error')); //...errors if any
  }
  

}