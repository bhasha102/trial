import { Component, OnInit } from '@angular/core';
import { Platform } from './platform';
import { ManageUserService} from './manageuser.service';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { UserList} from './user-list';
import { ChangeDetectorRef} from '@angular/core';

@Component({
  selector: 'platform',
  templateUrl: './manageuser.component.html',
  styleUrls: ['./manageuser.component.less']
})
export class ManageUserComponent implements OnInit {
errorMessage:string;
Platform:Platform[] ;
public myForm: FormGroup; 
responseObj :JSON;;
responseTxt:string;
userDetails: UserList[];
selectedState :any;
showtable = false;
showModal : boolean = false;
//radioItems = ['ADMIN','EAUTH ADMIN','MERCHANT USER','ORG ADMIN','USER'];
model = { options: 0 };
userId : string;
roles : Object[];
updatedMsg:boolean = false;
userRole :string;
sortVal : string;
sortType : string = 'Asc';
errorMsg : boolean = false;
    public constructor( private _fb: FormBuilder, private _adminService: ManageUserService, private changeDetectorRef: ChangeDetectorRef ) {
    }

  ngOnInit(){
       this.myForm = this._fb.group({
           adminPlatform : ['']
       }),
        this.getPlatformList();
        this.getRoles();
 }

     getPlatformList()
     {
        this._adminService.getPlatforms().subscribe(
                        platformDetails => this.Platform = platformDetails,
                        error =>  {console.log(error)});
                        
     }
   getRoles()
   {
       this._adminService.getRoles().subscribe(
                        roles => this.roles = roles,
                        error =>  {console.log(error)});
   }


  platformDetail(){
        console.log("inisde ");
    }

    onChange(plat){
        this.errorMsg = false;
        this.showtable = true;
        this._adminService.getUdetails().subscribe(
                        UserDetails => this.userDetails = UserDetails,
                        error =>  {console.log(error)});

          
     //   this._adminService.getUserDetails(plat).subscribe(
    //        response => this.responseObj = response,
     //       error => {console.log(error)}
     //   );
    }
     sort(type,key){
         this.errorMsg = false;
        if(type == 'Asc')
        {
            this.sortVal = key;
            this.sortType = 'Desc';
        }else if(type == 'Desc')
        {
            this.sortVal = "-"+key;
            this.sortType = 'Asc';
        }
    }

    deleteRowForm(){
        let j=0;
         for(let i =0;i< this.userDetails.length;i++)
         {
               if(this.userDetails[i].state)
                  j++;
         }
         if(j === 0)
         this.errorMsg = true;
         else
          this.errorMsg = false;
       
       this.userDetails = this.userDetails.filter(function(qt){
            console.log(qt.state);
              if (qt.state === false || qt.state == undefined)
                   return qt;
    })
      
}


changestatus(){
          let j=0;
         for(let i =0;i< this.userDetails.length;i++)
         {
               if(this.userDetails[i].state)
               {
                     if(this.userDetails[i].status === "Y"){
                         this.userDetails[i].status = "N";
                     }
                     else{
                        this.userDetails[i].status = "Y";
                    }
                    this.userDetails[i].state = false;
                        j++;
               }
         }
         if(j === 0)
         this.errorMsg = true;
         else
          this.errorMsg = false;
}

    useridactive(isvisible,item){
        this.userId = item.userId;
        this.showModal =isvisible;
        console.log(item);
        this.model.options = item != ''? item.roles[0].roleId : '';
        this.updatedMsg = false;
        this.errorMsg = false;
           
    }

    closeModal(event){
        console.log(event.target.className);
        if(event.target.className == "modal"){
            this.showModal =false;
        }
    }

    updateUserRole(newUserRole){
        console.log(newUserRole);
        // Send newuserRole to Service and update
         this.updatedMsg = true;
    }
}

