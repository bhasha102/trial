import { Injectable } from '@angular/core';
import { Platform} from './platform';
import { UserList} from './user-list';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ManageUserService{
//private baseUrl2: string = 'http://localhost:8085/dev-portal-intra/getUserData';
private baseUrl: string ="./app/admin/platform/admin.json";
private baseUrl1: string ="./app/admin/platform/user-list.json";
private baseUrl2: string ="./app/admin/platform/roles.json";
constructor(private http : Http){ }
  
  getPlatforms():Observable<Platform[]>
  {
       
      return this.http.get(this.baseUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     
  }
  getUdetails():Observable<UserList[]>
  {
       
      return this.http.get(this.baseUrl1)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     
  }

  getRoles():Observable<Object[]>
  {
      return this.http.get(this.baseUrl2)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  /* getUserDetails(body:string):Observable<JSON>{
        let bodyString = JSON.stringify({body}); // Stringify payload
       let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
      let options       = new RequestOptions({ headers: headers }); // Create a request option
        console.log(body);

let params = new URLSearchParams();
  params.set('orgid', body);

    return this.http.get(this.baseUrl2, {search: params})
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     
  }*/
        
}