import { Component, OnInit, Input, HostListener } from '@angular/core';

@Component({
  selector: 'amex-input',
  templateUrl: './amex-input.component.html',
  styleUrls: ['./amex-input.component.css']
})
export class AmexInputComponent implements OnInit {
  @Input() inputFormat;
  @Input() bindVariable;
  @Input() inputPlaceholder;
  @Input() inputLable;
  @Input() inputIcon;
  @Input() maxLength;
  @Input() minLength;
  @Input() limitChar;
  @Input() isRequired = false;
  @Input() isDisabled = false;
  @Input() onFocus;
  @Input() onBlur;
  @Input() onChange;
  @Input() actionBtnIcon;

  ngOnInit() {

    if(this.inputFormat ===""){
      this.inputFormat="text";
    }

  }
  @HostListener ('onchange',['$event'])
    onChangeInt(){
      this.onChange();
  }

}
