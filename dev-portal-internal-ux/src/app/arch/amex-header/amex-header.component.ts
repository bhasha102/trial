import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-amex-header',
  templateUrl: './amex-header.component.html',
  styleUrls: ['./amex-header.component.less']
})
export class AmexHeaderComponent implements OnInit {
   username : string = "Amex User";
  constructor() { }

  ngOnInit() {
  }

}
