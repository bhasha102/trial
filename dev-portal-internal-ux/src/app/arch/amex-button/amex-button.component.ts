import { Component, Input, HostListener, OnInit } from '@angular/core';
//import {amex-directive} from './amex-directive';
@Component({
  selector: 'amex-button',
  templateUrl: './amex-button.component.html',
  styleUrls: ['./amex-button.component.less']
})
export class AmexButtonComponent implements OnInit {
//parameter
@Input() callback = () => {};
@Input() btnLable = '';
@Input() btnType;
@Input() btnIcon;
@Input() btnOnlyIcon;
@Input() btnDisable=false;

//page load variable
btnText="go to google";
btnClass="";

ngOnInit() {
  if(this.btnType === "simple"){
    this.btnClass="btn-full-width btn-primary";
    this.btnIcon="";
  }else if(this.btnType === "simple-icon"){
    this.btnClass="btn-icon";
  }else if(this.btnOnlyIcon ==="icon"){
    this.btnLable="";
  }
}

@HostListener ('click',['$event'])
  callbackInt(){
    this.callback();
}
  
}
